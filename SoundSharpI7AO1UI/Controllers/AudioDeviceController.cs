﻿using SoundSharpI7AO1UI.Models;
using SoundSharpI7AO1UI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SoundSharpI7AO1UI.Controllers
{
    public class AudioDeviceController : Controller
    {
        // GET: AudioDevice
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MemoRecorder()
        {
            var memoRecorder = new VmMemoRecorder();
            return View(memoRecorder);
        }

        [HttpPost]
        public ActionResult MemoRecorder(VmMemoRecorder rec)
        {
            var newDevice = new AudioDevice();
            newDevice.Make = rec.Make;
            newDevice.Model = rec.Model;

            var newMemo = new MemoRecorder();
            newMemo.MaxCartridgeType = rec.MaxCartridgeType;

            return View();
        }
    }
}