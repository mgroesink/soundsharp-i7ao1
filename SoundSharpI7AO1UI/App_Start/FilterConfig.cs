﻿using System.Web;
using System.Web.Mvc;

namespace SoundSharpI7AO1UI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
