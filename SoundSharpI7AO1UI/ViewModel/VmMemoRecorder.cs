﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SoundSharpI7AO1UI.ViewModel
{
    public class VmMemoRecorder
    {
        [Key]
        public int Id { get; set; }
        public string Model { get; set; }
        public string Make { get; set; }
        public decimal PriceExBtw { get; set; }
        public System.DateTime CreationDate { get; set; }
        public decimal BtwPercentage { get; set; }
        public string MaxCartridgeType { get; set; }
    }
}