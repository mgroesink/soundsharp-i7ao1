﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SoundSharpI7AO1UI.Startup))]
namespace SoundSharpI7AO1UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
